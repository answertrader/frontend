import React, { useState } from 'react'
import { Icon, Input } from 'antd';
import Router from 'next/router'
import styled from 'styled-components'

const QInput = styled(Input)`
    border-radius: 0;
`;

const AskField = () => {

    let [value, set_value] = useState("");

    let submit = () => Router.push({
        pathname: '/ask',
        query: {title: value}
    })

    return (
        <QInput
            size="large"
            placeholder="Ask a question, or search existing questions..."
            onChange={(e) => set_value(e.target.value)}
            onPressEnter={e => submit()}
            suffix={ 
                <Icon 
                    onClick={e => submit()}
                    type='arrow-right'
                /> 
            }
        />
    )

}

export default AskField