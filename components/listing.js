import React from 'react'
import styled from 'styled-components'
import Link from 'next/link';
import Bounty from '../components/bounty'

const Title = styled.a`
    color: inherit;
    font-weight: bold;
    font-size: 1.3rem;
    padding: 0 3rem;
`;
const Item = styled.li`
    background-color: rgba(0,0,0,0.1);
    display: flex;
    align-items: center;
    padding: 1.5rem;
    margin-bottom: 1rem;
    &:last-child {
        margin-bottom: 0;
    }
`;
const Listing = ({question}) => (
    <Item>
        <Bounty amount={question.bounty_amount}/>
        <Link href={`/question?_id=${question._id}`}>
            <Title>
                {question.title}
            </Title>
        </Link>
    </Item>
)

export default Listing