import Link from 'next/link'
import styled from 'styled-components'
import { Icon } from 'antd'

const Flex = styled.div`
    display: flex;
    padding-top: 1rem;
    padding-bottom: 2rem;
`;
const Title = styled.h2`
    font-weight: bold;
    margin-bottom: 0;
`;

const BackArrow = styled(Icon)`
    font-size: 3rem;
    margin-right: 3rem;
`;

const TopBar = ({title, noback}) => (
    <Flex>
        { 
            noback ? null : (
                <Link href='/'>
                    <BackArrow type='arrow-left'/>
                </Link> 
            )
        }
        <Title>
            { title }
        </Title>
    </Flex>
)

export default TopBar
