import React, { useState, useEffect } from 'react'
import axios from 'axios'
import styled from 'styled-components'
import Listing from './listing'

const List = styled.ul`
    padding: 0;
    list-style-type: none;
    margin: 0;
`;
const QuestionList = () => {

    let [questions, set_questions] = useState([]);

    useEffect( () => {
        const fetch_data = async () => {
            const data = await axios.get(
                'http://localhost:4000/questions'
            ).then(res => res.data)

            set_questions(data)
        }
        fetch_data()
    }, [])

    return (
        <List>
            {
                questions.map( q => 
                    <Listing key={q._id} question={q}/>
                )
            }
        </List>
    )
}

export default QuestionList