import React from 'react'
import styled from 'styled-components'

const Square = styled.div`
    background-color: rgba(0,0,0,0.2);
    color: black;
    padding: 1.5rem;
    text-align: center;
    min-width: 75px;
    position: relative;
`;
const Unit = styled.div`
    font-size: 1rem;
`;
const Bounty = ({amount}) => (
    <Square>
        <data>
            <b>{amount}</b>
            <Unit>ATC</Unit>
        </data>
    </Square>
)

export default Bounty