import React from 'react'
import {Button} from 'antd'
import { auth } from '../auth/auth'

const login_button = () => (
    <div>
    {
        !auth.user ? (
            <Button onClick={ e => auth.sign_in() } >
                Login
            </Button>
        ) : (
            <Button onClick={ e => auth.sign_out()} >
                Logout
            </Button>
        )
    }
    </div>
)

export default login_button
