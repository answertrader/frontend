import React, { useState } from 'react'
import styled from 'styled-components'
import Box from './box'
import { Input, Button, Icon, Tooltip } from 'antd'
import TopBar from './top_bar'
import axios from 'axios'

const Right = styled.div`
    direction: rtl;
    margin-top: 1.5rem;
`;

const Label = styled.label`
    font-weight: bold;
    font-size: 1rem;
`;

const AnswerForm = ({question_id}) => {
    let [content, set_content] = useState("");
    let [reward_address, set_reward_address] = useState("");

    let submit = () => {
        console.log(content);
        axios({
            method: 'post',
            url: `http://127.0.0.1:4000/answer/${question_id}`,
            data: {content, reward_address}
        }).catch(error => console.error(error))
    }

    return (
        <Box>
            <TopBar title="Post a response" noback />
            <Input.TextArea
                style={{resize: 'none'}}
                autosize={{minRows: 4}}
                placeholder="Write your response here. Try to be helpful and comprehensive. Include citations if the question demands it."
                onChange={e => set_content(e.target.value)}
            />
            <Input
                style={{boxSizing: "border-box", margin: "1rem 0"}}
                placeholder="Your ATC wallet address"
                onChange={e => set_reward_address(e.target.value)}
                suffix={
                    <Tooltip title="Address to which bounty payment will be sent (if your answer is accepted).">
                        <Icon 
                            type="info-circle" 
                            style={{ color: 'rgba(0,0,0,.45)' }} 
                        />
                    </Tooltip>
                }
            />
            <Right>
                <Button 
                    onClick={e => submit()}
                    type='primary'
                >
                    Submit
                </Button>
            </Right>
        </Box>
    )
}
export default AnswerForm