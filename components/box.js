import styled from 'styled-components'
const Box = styled.div`
    background-color: white;
    border-radius: 10px;
    margin: 4rem 0;
    padding: ${props => props.compact ? '2' : '4'}rem;
    @media (max-width: 1000px) {
        padding: 2rem;
    }
    @media (max-width: 400px) {
        padding: 1rem;
    }
`;
export default Box