import auth0 from 'auth0-js';
import { save_item, get_item } from '../utils'
import jwt_decode from 'jwt-decode'
import crypto from 'crypto'
import Cookies from 'js-cookie'

class Auth {
    constructor() {
        this.auth0 = new auth0.WebAuth({
            domain: 'answertrader.eu.auth0.com',
            clientID: 'KGgfbzy6gxMAyM2mtGkSBhPWd67QpSii',
            redirectUri: 'http://localhost:3000/callback',
            audience: "asklet-backend",
            responseType: 'token',
            scope: 'openid'
        });

        // Client starts, loads access_token from cookie (if not expired)
        let existing_token = Cookies.get('access_token') 
        console.log(`Found existing access token: ${existing_token}`)
        if (existing_token && !is_expired(existing_token)) {
            console.log("It's still good - using now")
            this.load_into_memory(existing_token)
        }
    }
    load_into_memory = access_token => {
        this.access_token = access_token
        this.user = jwt_decode(access_token).sub
    }
    save_token(new_token) {
        this.load_into_memory(new_token)
        console.log("Token details on save: ") 
        const token_details = jwt_decode(new_token)
        console.log(token_details) 
        Cookies.set('access_token', new_token, {
            expires: new Date(token_details.exp * 1000)
        })
    }
    sign_in() {
        this.auth0.authorize()
    }
    sign_out() {
        Cookies.remove('access_token')
        this.access_token = null;
        this.user = null;
    }
    sign_in_preserve_state(app_state = {}) {
        // What should the protocol be for state preservation?
        /*
            Currently, callback checks if the state is a key to localStorage
            If so, treat the data there as state to be resumed
            That state should be an object like 
            {
                post_in_progress: {
                    // post properties...
                }
            }
            in which the top-level property of the state object refers to 
            the context which was redirected from (in this case, post submission)
        */
        let state_key = gen_nonce();
        save_item(state_key, app_state)
        auth.auth0.authorize({
            state: state_key
        })
    }
    get_auth_header = () => ({ // check if access_token has expired
        headers: {'Authorization': `bearer ${this.access_token}`}
    })
}
const gen_nonce = () => crypto.randomBytes(5).toString('hex')
function is_expired(access_token) {
    let expiry = jwt_decode(access_token).exp * 1000;
    let current_unixtime = (new Date).getTime();

    console.log(`Current time: ${current_unixtime}`)
    console.log(`Expiration time: ${expiry}`)

    return current_unixtime > expiry
}
export const auth = new Auth();
export const get_query_params = () => {
    const params = {};
    window.location.href.replace(
        /([^(?|#)=&]+)(=([^&]*))?/g,
        (_, $1, __, $3) => { params[$1] = $3 }
    );
    return params;
};