import React from 'react'
import { Input, Form, Button } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import { withRouter } from 'next/router'
import TopBar from '../components/top_bar'
import styled from 'styled-components'
import {auth} from '../auth/auth'
import crypto from 'crypto'
import { save_item } from '../utils'

const Right = styled.div`
    direction: rtl;
`;


class Compose extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            title: props.router.query.title ? props.router.query.title : "",
            description: ""
        }
    }

    save() {

    }
    render(props) {
        return <div>
            <TopBar title="What do you want to know?" />
            <Form>
                <Form.Item label="Title">
                    <Input 
                        placeholder="What's your question?"
                        value={this.state.title}
                        onChange={e => this.setState({ title: e.target.value})}
                    />
                </Form.Item>
                <Form.Item label="Description"> 
                    <TextArea 
                        placeholder="Provide details..."
                        value={this.state.description}
                        onChange={e => this.setState({description: e.target.value})}
                        autosize={{minRows: 4}}
                        style={{resize: "none"}}
                    />
                </Form.Item>

                <Right>
                    <Button 
                        onClick={e => { 
                            auth.sign_in_preserve_state({
                                post_in_progress: this.state
                            })

                        }}
                        type="primary" 
                        htmlType="submit"
                    >
                        Next (Sign-up/Login)
                    </Button>
                </Right>
            </Form>
        </div>
    }
}

export default withRouter(Compose)