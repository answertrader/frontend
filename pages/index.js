import React from 'react'
import AskField from '../components/ask_field';
import QuestionList from '../components/question_list'
import styled from 'styled-components'
import Box from '../components/box'

const Center = styled.div`
    text-align: center;
`;
const Tagline = styled.h2`
    margin-bottom: 1.2rem;
    font-weight: bold;
`;

const Home = () => <div>
        <Box>
            <Center>
                <Tagline>
                    What do you want to know?
                </Tagline>
                <AskField/>
            </Center>
        </Box>
        <Box compact>
            <QuestionList/>
        </Box>
</div>

export default Home