import React from 'react'
import { PageHeader } from 'antd';
import QuestionList from '../components/question_list';
import Router from 'next/router'

export default props => (
    <PageHeader
        onBack={() => Router.push('/')}
        title="Question Listings"
    >
        <QuestionList />
    </PageHeader>
)