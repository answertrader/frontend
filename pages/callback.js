import React from 'react'
import { auth, get_query_params } from '../auth/auth'
import { get_item } from '../utils'
import Router from 'next/router'
import axios from 'axios'

/*
    Callback basically just sets access token cookie, 
    then redirects the user, according to the 'state' param
*/
class Callback extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const { access_token, state } = get_query_params();

        auth.save_token(access_token);

        let prev_state = get_item(state);
         
        if (prev_state) {
            if (prev_state.post_in_progress) { 
                let post = prev_state.post_in_progress;
                axios.post(
                    'http://localhost:4000/question',
                    post,
                    auth.get_auth_header()
                ).catch(err => console.error(err))
                .then(res => Router.push(`/question?_id=${res.data.new_question_id}`))
            } else {
                console.log(`Unrecognised redirect state`)
            }

        } else {
            console.log("Regular login - Redirecting to /")
            Router.push('/');
        }
    }
    render() {
        return (
            <div>
                Loading
            </div>
        )
    }
}

export default Callback
