import React from 'react'
import { Button, Statistic } from 'antd';
import {withRouter} from 'next/router'
import axios from 'axios';
import styled from 'styled-components'
import Box from '../components/box'
import AnswerForm from '../components/answer_form'
import TopBar from '../components/top_bar'
import { auth } from '../auth/auth'

const Right = styled.div`
    text-align: center;
    margin-top: 1rem;
`;

const Content = styled.div`
    padding: 2rem 4rem;
`;

const Question = ({question}) => (
    <div>
        <Box>
            <TopBar title={question.title} />
            <Content>
                {/* Temporary - will either not include name, or */}
                <h5>{question.author}</h5>
                {question.description}
            </Content>
            <Right>
                <Statistic
                    title="Bounty (ATC)"
                    value={question.bounty_amount}
                />
            </Right>
        </Box>
        <AnswerForm question_id={question._id} />
        {
            question.answers.map( a => (
                <Box key={a._id}>
                    <article>
                        {a.content}
                    </article>
                    <div>{
                        a.accepted ? "Accepted!" : null
                    }</div>
                    {
                        auth.user === question.author ? <Button
                            onClick={e => {

                                axios.post(
                                    `http://localhost:4000/accept/${question._id}`,
                                    {
                                        answer_id: a._id
                                    },
                                    auth.get_auth_header()
                                )

                            }}
                        >
                            Accept
                        </Button> : null
                    }
                </Box>
            ))
        }
    </div>
)

Question.getInitialProps = async (context) => {

    const question = await axios
        .get(`http://localhost:4000/question/${context.query._id}`)
        .then(res => res.data)

    return {
        question
    }
}

export default withRouter(Question)