import React from 'react'
import styled from 'styled-components'
import Head from 'next/head'
import { TITLE } from '../constants'
import LoginButton from '../components/login_button'


const Bg = styled.div`
`;
const Column = styled.div`
    padding: 10rem 1.8rem;
    max-width: 900px;
    margin: auto;
`;
const TopBar = styled.div`
    width: 100vw;
    padding: 1.5rem 2rem;
`;
const Logo = styled.h1`
    font-size: 1.5rem;
    color: black;
`;

export default ({children}) => (
    <Bg>
        <Head>
            <title>AnswerTrader</title>
        </Head>
        <TopBar>
            <Logo>
                {TITLE}
            </Logo>
            <LoginButton />
        </TopBar>
        <Column>
            {children}
        </Column>
    </Bg>
)